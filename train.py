import torch as T
import torch.nn as nn
import torch.nn.functional as F

import numpy as np
import os
from dataset import get_iterators
from helper_functions import get_cuda, get_sentences_in_batch
import math
from tqdm import notebook, tqdm


device = "cuda"
#criterion = nn.CrossEntropyLoss().cuda() if torch.cuda.is_available() else nn.CrossEntropyLoss()

def train_batch(x, G_inp, step, train = True):
    with T.backends.cudnn.flags(enabled=False):
        if train == True:                                    #skip below step if we are performing validation
            with T.autograd.set_detect_anomaly(True):
                z = T.zeros(x.shape[0], opt.n_z).to(device).requires_grad_()
                logit, _, kld = vae(x.to(device), G_inp.to(device), z, None)
                logit = logit.view(-1, opt.n_vocab)	                    #converting into shape (batch_size*(n_seq-1), n_vocab) to facilitate performing F.cross_entropy()
                x_tmp = x[:, 1:x.size(1)]	                                #target for generator should exclude first word of sequence
                x_tmp = x_tmp.contiguous().view(-1)	                            #converting into shape (batch_size*(n_seq-1),1) to facilitate performing F.cross_entropy()
                rec_loss = F.cross_entropy(logit.to(device), x_tmp.to(device))
                kld_coef = (math.tanh((step - 15000)/500) + 1) / 2
                #kld_coef = min(1,step/(200000.0))
                inner_loss = opt.rec_coef*rec_loss + kld_coef*kld

                grad = T.autograd.grad(inner_loss, [z], create_graph=True, retain_graph=True)[0]
                z = (-grad)

                logit, _, kld = vae(x.to(device), G_inp.to(device), z, None)
                logit = logit.view(-1, opt.n_vocab)	                    #converting into shape (batch_size*(n_seq-1), n_vocab) to facilitate performing F.cross_entropy()
                x = x[:, 1:x.size(1)]	                                #target for generator should exclude first word of sequence
                x = x.contiguous().view(-1)                             #converting into shape (batch_size*(n_seq-1),1) to facilitate performing F.cross_entropy()
                rec_loss = F.cross_entropy(logit.to(device), x.to(device))
                outer_loss = opt.rec_coef*rec_loss + kld_coef*kld

                trainer_vae.zero_grad()
                outer_loss.backward()
                trainer_vae.step()
        else:
            z = T.zeros(x.shape[0], opt.n_z).to(device)
            logit, _, kld = vae(x.to(device), G_inp.to(device), z, None)
            logit = logit.view(-1, opt.n_vocab)	                    #converting into shape (batch_size*(n_seq-1), n_vocab) to facilitate performing F.cross_entropy()
            x_tmp = x[:, 1:x.size(1)]	                                #target for generator should exclude first word of sequence
            x_tmp = x_tmp.contiguous().view(-1)	                            #converting into shape (batch_size*(n_seq-1),1) to facilitate performing F.cross_entropy()
            rec_loss = F.cross_entropy(logit.to(device), x_tmp.to(device))
            #kld_coef = (math.tanh((step - 7500)/500) + 1) / 2
            
    return rec_loss.item(), kld.item()


def str2bool(v):
    if v.lower() == 'true':
        return True
    else:
        return False
    
def create_generator_input(x, train):
    G_inp = x[:, 0:x.size(1)-1].clone()	                    #input for generator should exclude last word of sequence
    if train == False:
        return G_inp

    r = np.random.rand(G_inp.size(0), G_inp.size(1))
                                                            #Perform word_dropout according to random values (r) generated for each word
    for i in range(len(G_inp)):
        for j in range(1,G_inp.size(1)):
            if r[i, j] < opt.word_dropout and G_inp[i, j] not in [vocab.stoi[opt.pad_token], vocab.stoi[opt.end_token]]:
                G_inp[i, j] = vocab.stoi[opt.unk_token]

    return G_inp

def load_model_from_checkpoint():
    global vae, trainer_vae
    checkpoint = T.load(save_path)
    vae.load_state_dict(checkpoint['vae_dict'])
    trainer_vae.load_state_dict(checkpoint['vae_trainer'])
    return checkpoint['step'], checkpoint['epoch']


class ResBiLSTM(nn.Module):

    def __init__(self, input_size, hidden_size, num_layers, batch_first = True):
        super(ResBiLSTM, self).__init__()

        self.hidden_size = hidden_size
        self.input_size = input_size

        self.ResBlockLSTM = nn.ModuleList([

                nn.LSTM(
                    input_size= input_size, #if _ == 0 else hidden_size),
                    hidden_size=hidden_size,
                    num_layers=1,
                    batch_first=batch_first,
                    bidirectional=True
                )
                
                for _ in range(num_layers)
            ])
        self.ResBlockLinear = nn.ModuleList([
            nn.Linear(hidden_size*2, input_size) for _ in range(num_layers)
            ])

    def forward(self, x, hidden):
        #print(x)
        #lstm_input = x
        hn, cn = hidden

        res_out = get_cuda(T.zeros((x.shape[0], x.shape[1], self.input_size)))
        
        res_out += x

        for i, (lstm, linear) in enumerate(zip(self.ResBlockLSTM, self.ResBlockLinear)):
            #if i == 0:
                #out, (hn, cn) = lstm(res, (hn, cn))
            #else:
            out, (hn, cn) = lstm(res_out, (hn, cn))
            #lstm_hidden = (hn, cn)
            #print(res_out.shape)
            res_out += linear(out)
            #lstm_input = res_out

        return res_out, (hn, cn)


class GeneratorRES(nn.Module):
    def __init__(self, opt):
        super(GeneratorRES, self).__init__()
        self.n_hidden_G = opt.n_hidden_G
        self.n_layers_G = opt.n_layers_G
        self.n_z = opt.n_z
        self.lstm = ResBiLSTM(input_size=opt.n_embed+opt.n_z, hidden_size=opt.n_hidden_G, num_layers=opt.n_layers_G, batch_first=True)
        self.fc = nn.Linear(opt.n_embed+opt.n_z, opt.n_vocab)

    def init_hidden(self, batch_size):
        h_0 = T.zeros(2, batch_size, self.n_hidden_G)
        c_0 = T.zeros(2, batch_size, self.n_hidden_G)
        self.hidden = (get_cuda(h_0), get_cuda(c_0))

    def forward(self, x, z, g_hidden = None):
        batch_size, n_seq, n_embed = x.size()
        z = T.cat([z]*n_seq, 1).view(batch_size, n_seq, self.n_z)	#Replicate z inorder to append same z at each time step
        x = T.cat([x,z], dim=2)	                                    #Append z to generator word input at each time step

        if g_hidden is None:	                                    #if we are validating
            self.init_hidden(batch_size)
        else:					                                    #if we are training
            self.hidden = g_hidden

        #Get top layer of h_T at each time step and produce logit vector of vocabulary words
        #print(x, self.lstm)
        output, self.hidden = self.lstm(x, self.hidden)
        output = self.fc(output)

        return output, self.hidden
 


class Generator(nn.Module):
    def __init__(self, opt):
        super(Generator, self).__init__()
        self.n_hidden_G = opt.n_hidden_G
        self.n_layers_G = opt.n_layers_G
        self.n_z = opt.n_z
        self.lstm = nn.LSTM(input_size=opt.n_embed+opt.n_z, hidden_size=opt.n_hidden_G, num_layers=opt.n_layers_G, batch_first=True, bidirectional=True)
        self.fc = nn.Linear(opt.n_hidden_G*2, opt.n_vocab)

    def init_hidden(self, batch_size):
        h_0 = T.zeros(self.n_layers_G*2, batch_size, self.n_hidden_G)
        c_0 = T.zeros(self.n_layers_G*2, batch_size, self.n_hidden_G)
        self.hidden = (get_cuda(h_0), get_cuda(c_0))

    def forward(self, x, z, g_hidden = None):
        batch_size, n_seq, n_embed = x.size()
        z = T.cat([z]*n_seq, 1).view(batch_size, n_seq, self.n_z)	#Replicate z inorder to append same z at each time step
        x = T.cat([x,z], dim=2)	                                    #Append z to generator word input at each time step

        if g_hidden is None:	                                    #if we are validating
            self.init_hidden(batch_size)
        else:					                                    #if we are training
            self.hidden = g_hidden

        #Get top layer of h_T at each time step and produce logit vector of vocabulary words
        output, self.hidden = self.lstm(x, self.hidden)
        output = self.fc(output)

        return output, self.hidden
    
class Net(nn.Module):
    def __init__(self, n_z, hidden_states, out_dim):
        super(Net, self).__init__()
        self.lin1 = nn.Linear(n_z, hidden_states)
        self.lin2 = nn.Linear(hidden_states, hidden_states)
        self.lin3 = nn.Linear(hidden_states, out_dim)

    # x represents our data
    def forward(self, x):
      # Pass data through conv1
        x = self.lin1(x)
      # Use the rectified-linear activation function over x
        x = F.relu(x)

        x = self.lin2(x)
        x = F.relu(x)
        
        x = self.lin3(x)
        #x = F.relu(x)


      # Apply softmax to x
      #output = F.log_softmax(x, dim=1)
        return x

class Encoder(nn.Module):
    def __init__(self, opt):
        super(Encoder, self).__init__()
        self.highway = Highway(opt)
        self.n_hidden_E = opt.n_hidden_E
        self.n_layers_E = opt.n_layers_E
        self.lstm = nn.LSTM(input_size=opt.n_embed, hidden_size=opt.n_hidden_E, num_layers=opt.n_layers_E, batch_first=True, bidirectional=True)

    def init_hidden(self, batch_size):
        h_0 = T.zeros(2*self.n_layers_E, batch_size, self.n_hidden_E)
        c_0 = T.zeros(2*self.n_layers_E, batch_size, self.n_hidden_E)
        self.hidden = (get_cuda(h_0), get_cuda(c_0))

    def forward(self, x):
        batch_size, n_seq, n_embed = x.size()
        x = self.highway(x)
        self.init_hidden(batch_size)
        _, (self.hidden, _) = self.lstm(x, self.hidden)	             #Exclude c_T and extract only h_T
        self.hidden = self.hidden.view(self.n_layers_E, 2, batch_size, self.n_hidden_E)
        self.hidden = self.hidden[-1]	                             #Select only the final layer of h_T
        e_hidden = T.cat(list(self.hidden), dim=1)	                 #merge hidden states of both directions; check size
        return e_hidden
    
    
class GON_text(nn.Module):
    def __init__(self, opt, training = True):
        super(GON_text, self).__init__()
        self.embedding = nn.Embedding(opt.n_vocab, opt.n_embed)
        self.embedding.to(device)
        #self.encoder = Encoder(opt)
        #self.hidden_to_mu = nn.Linear(2*opt.n_hidden_E, opt.n_z)
        #self.hidden_to_logvar = nn.Linear(2*opt.n_hidden_G, opt.n_z)
        
        #self.generator = Generator(opt)
        self.generator = GeneratorRES(opt)
        self.n_z = opt.n_z
        self.fc21 = Net(self.n_z, self.n_z*3, self.n_z)
        self.fc22 = Net(self.n_z, self.n_z*3, self.n_z)
        self.training = training

    def reparameterize(self, mu, logvar):
        if self.training:
            std = logvar.mul(0.5).exp_()
            eps = std.data.new(std.size()).normal_()
            return eps.mul(std).add_(mu)
        else:
            return mu

    def forward(self, x, G_inp, z_gon, G_hidden = None, Train = True):
        #if z is None:                                                   #If we are testing with z sampled from random noise
        #print(x.size())
        batch_size, n_seq = x.size()
        #x = self.embedding(x)                                           #Produce embeddings from encoder input
        #E_hidden = self.encoder(x)                                     #Get h_T of Encoder
        mu = self.fc21(z_gon)#self.hidden_to_mu(E_hidden)               #Get mean of lantent z
        logvar = self.fc22(z_gon)#self.hidden_to_logvar(E_hidden)       #Get log variance of latent z
        z = get_cuda(T.randn([batch_size, self.n_z]))                   #Noise sampled from ε ~ Normal(0,1)
        #print(z.size(), logvar.size(), mu.size())
        z = mu + z*T.exp(0.5*logvar)                                    #Reparameterization trick: Sample z = μ + ε*σ for backpropogation
        
        kld = -0.5*T.sum(logvar-mu.pow(2)-logvar.exp()+1, 1).mean()     #Compute KL divergence loss
        
        #else:
            #kld = None                                                  #If we are training with given text

        G_inp = self.embedding(G_inp)                                   #Produce embeddings for generator input

        logit, G_hidden = self.generator(G_inp, z, G_hidden)
        return logit, G_hidden, kld
    
    

def training():
    start_epoch = step = 0
    if opt.resume_training:
        step, start_epoch = load_model_from_checkpoint()
    lowest_lost = 10000
    for epoch in tqdm(range(start_epoch, opt.epochs)):
        vae.train()
        train_rec_loss = []
        train_kl_loss = []
        tqdm_iter =tqdm(train_iter)
        
        for batch in tqdm_iter:
            #if step>1:break
            #try:
            x = batch.text 	                                #Used as encoder input as well as target output for generator
            G_inp = create_generator_input(x, train = True)
            rec_loss, kl_loss = train_batch(x, G_inp, step, train=True)
            train_rec_loss.append(rec_loss)
            train_kl_loss.append(kl_loss)
            step += 1
            tqdm_iter.set_description(f"rec loss: {sum(train_rec_loss)/len(train_rec_loss)} kld-loss: { sum(train_kl_loss)/len(train_kl_loss)}")
            if (step+1) % 200 == 0 and (rec_loss+kl_loss) < lowest_lost:
                lowest_lost = (rec_loss+kl_loss)
                T.save({
                    'epoch': epoch + 1,
                    'vae_dict': vae.state_dict(),
                    'vae_trainer': trainer_vae.state_dict(),
                    'loss': float(rec_loss+kl_loss),
                    'step': step
                }, save_path)
                
        
            #except:
                #continue

        vae.eval()
        valid_rec_loss = []
        valid_kl_loss = []
        val_tqdm_iter = tqdm(val_iter)
        for batch in val_tqdm_iter:
            try:
                x = batch.text
                G_inp = create_generator_input(x, train = False)
                with T.autograd.no_grad():
                    rec_loss, kl_loss = train_batch(x, G_inp, step, train=False)
            except: continue
            valid_rec_loss.append(rec_loss)
            valid_kl_loss.append(kl_loss)
            val_tqdm_iter.set_description(f"loss: {sum(valid_rec_loss)/len(valid_rec_loss) + sum(valid_kl_loss)/len(valid_kl_loss)}")

        train_rec_loss = np.mean(train_rec_loss)
        train_kl_loss = np.mean(train_kl_loss)
        valid_rec_loss = np.mean(valid_rec_loss)
        valid_kl_loss = np.mean(valid_kl_loss)

        print("No.", epoch, "T_rec:", '%.2f'%train_rec_loss, "T_kld:", '%.2f'%train_kl_loss, "V_rec:", '%.2f'%valid_rec_loss, "V_kld:", '%.2f'%valid_kl_loss)
        if epoch%5==0:
            T.save({
                'epoch': epoch + 1,
                'vae_dict': vae.state_dict(),
                'vae_trainer': trainer_vae.state_dict(),
                'step': step
            }, save_path)

def generate_sentences(n_examples):                             #Generate n sentences
    checkpoint = T.load(save_path)
    vae.load_state_dict(checkpoint['vae_dict'])
    vae.eval()
    del checkpoint
    for i in range(n_examples):
        #z = get_cuda(T.randn([1,opt.n_z]))
        z = T.zeros(opt.batch_size, opt.n_z).to(device).requires_grad_()
        h_0 = get_cuda(T.zeros(opt.n_layers_G*2, 1, opt.n_hidden_G))
        c_0 = get_cuda(T.zeros(opt.n_layers_G*2, 1, opt.n_hidden_G))
        G_hidden = (h_0, c_0)
        G_inp = T.LongTensor(1,1).fill_(vocab.stoi[opt.start_token])
        G_inp = get_cuda(G_inp)
        str = opt.start_token+" "
        while G_inp[0][0].item() != vocab.stoi[opt.end_token]:
            with T.autograd.no_grad():
                logit, G_hidden, _ = vae(None, G_inp, z, G_hidden)
            probs = F.softmax(logit[0], dim=1)
            G_inp = T.multinomial(probs,1)
            str += (vocab.itos[G_inp[0][0].item()]+" ")
        print(str.encode('utf-8'))
        

class Config():
    def __init__(self):
        self.batch_size = 256
        self.n_vocab = 12000
        self.epochs = 121
        self.n_hidden_G = 128
        self.n_layers_G = 3
        self.n_z = 100
        self.word_dropout = 0.5
        self.n_embed = 300
        self.rec_coef = 7
        self.lr = 1e-5
        self.gpu_device = 0
        self.unk_token = "<unk>"
        self.pad_token = "<pad>"
        self.start_token = "<sos>"
        self.end_token = "<eos>"
        self.resume_training = False
        self.to_train = True
        #self.word_dropout = 0.1
        
opt = Config()

save_path = "data/saved_models/vae_model.tar"
if not os.path.exists("data/saved_models"):
    os.makedirs("data/saved_models")

os.environ["CUDA_VISIBLE_DEVICES"] = str(opt.gpu_device)    #str(gpu_device)
train_iter, val_iter, vocab = get_iterators(opt)

vae = GON_text(opt)
vae.embedding.weight.data.copy_(vocab.vectors)              #Intialize trainable embeddings with pretrained glove vectors

vae = get_cuda(vae)
trainer_vae = T.optim.Adam(vae.parameters(), lr=opt.lr)

if __name__ == '__main__':
    if opt.to_train:
        training()
    else:
        generate_sentences(50)
