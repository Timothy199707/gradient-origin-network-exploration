import torch
import torch.nn as nn

#from nvae.decoder import Decoder
from nvae.decoder_small import Decoder
#from nvae.encoder import Encoder
from nvae.losses import recon, kl
from nvae.utils import reparameterize


    
class NGON(nn.Module):

    def __init__(self, z_dim, img_dim, latent, M_N=0.005):
        self.img_dim = img_dim
        self.z_dim = z_dim
        super().__init__()

        self.M_N = M_N
        #self.fc11 = nn.Linear(latent, z_dim)
        #self.fc12 = nn.Linear(latent, z_dim)
        #self.fc22 = nn.Linear(latent, z_dim//2 * img_dim//16 * img_dim//16)
        #self.fc33 = nn.Linear(latent, z_dim//8 * img_dim//4 * img_dim//4)
        self.implicit_net = nn.ModuleList([
                #nn.Linear(latent, z_dim),
                nn.Linear(latent, z_dim//2 * img_dim//16 * img_dim//16),
                nn.Linear(latent, z_dim//8 * img_dim//4 * img_dim//4)
            ])
        self.mu_net=nn.Linear(latent, z_dim)
        self.sigma_net=nn.Linear(latent, z_dim)
        '''self.mu_net = nn.ModuleList([
                #nn.Linear(latent, z_dim),
                nn.Linear(latent, z_dim//2 * img_dim//16 * img_dim//16),
                nn.Linear(latent, z_dim//8 * img_dim//4 * img_dim//4)
            ])
        
        self.sigma_net = nn.ModuleList([
                #nn.Linear(latent, z_dim),
                nn.Linear(latent, z_dim//2 * img_dim//16 * img_dim//16),
                nn.Linear(latent, z_dim//8 * img_dim//4 * img_dim//4)
            ])'''
        #self.encoder = Encoder(z_dim)
        self.decoder = Decoder(z_dim)
        self.img_dim = img_dim

    def forward(self, x, z_origins, Batch, z_dim, train=True, kl_step=True):
        """

        :param x: Tensor. shape = (B, C, H, W)
        :return:
        """
        #para_list = [(mu(z_), sigma(z_)) for _, (z_, mu, sigma) in enumerate(zip(z_origins[1:], self.mu_net, self.sigma_net))]
        zs = [imp_net(z_) for _, (z_, imp_net) in enumerate(zip(z_origins[1:], self.implicit_net))]
        #z0 = self.implicit_net(z_origins[0])
        mu, log_var = self.mu_net(z_origins[0]),  self.sigma_net(z_origins[0])
        z0 = reparameterize(mu, torch.exp(0.5 * log_var))
        
        #zs = [
            #reparameterize(mu, torch.exp(0.5 * sigma)) for _, (mu, sigma) in enumerate(para_list)
        #]
        #print(zs[1])
        
        #mu, log_var = self.fc11(z_origin[0]), self.fc12(z_origin[0])
        #mu, log_var = self.fc11(z_origin), self.fc12(z_origin)

        #hidden1 = self.fc21(z_origin[1]).reshape((Batch, z_dim//2, img_dim[0]//16, img_dim[1]//16))
        #hidden2 = self.fc31(z_origin[2]).reshape((Batch, z_dim//8, img_dim[0]//4, img_dim[1]//4))
        #zs[0] = zs[0].reshape((Batch, z_dim, self.img_dim//32, self.img_dim//32))
        #zs[1] = zs[1].reshape((Batch, z_dim//2, self.img_dim//16, self.img_dim//16))
        #zs[2] = zs[2].reshape((Batch, z_dim//8, self.img_dim//4, self.img_dim//4))
        zs[0] = zs[0].reshape((Batch, z_dim//2, self.img_dim//16, self.img_dim//16))
        zs[1] = zs[1].reshape((Batch, z_dim//8, self.img_dim//4, self.img_dim//4))
                
        #xs = [hidden1, hidden2]
        
        # (B, D_Z)
        
        #z = reparameterize(mu, torch.exp(0.5 * log_var))

        decoder_output, losses = self.decoder(z0, zs)
        #decoder_output, losses = self.decoder(zs[0], zs[1:])
        #print(decoder_output.shape)
        kl_total_loss = 0
        if train:
            recon_loss = recon(decoder_output, x)
            #mu, log_var = para_list[0]
            kl_loss = kl(mu, log_var)
            
            kl_total_loss = kl_loss + (1 / 2 * losses[0] + 1 / 8 * losses[1])
            if kl_step:
                
                #vae_loss = recon_loss + self.M_N * (kl_loss + 1 / 2 * losses[0] + 1 / 8 * losses[1])
                vae_loss = recon_loss + self.M_N * kl_total_loss#(1 / 2 * losses[0] + 1 / 8 * losses[1])
            else:
                
                vae_loss = recon_loss
        else:vae_loss = 0.0
            
        #vae_loss = recon_loss + self.M_N * (kl_loss)

        return decoder_output, vae_loss, kl_total_loss


#if __name__ == '__main__':
    #vae = NVAE(512, (64, 64))
    #img = torch.rand(2, 3, 64, 64)
    #img_recon, vae_loss = vae(img)
    #print(img_recon.shape)
    #print(vae_loss)
