import argparse
import logging
import os
import time

import matplotlib.pyplot as plt
import numpy as np
import torch
from torch.utils.data import DataLoader

from nvae.dataset import ImageFolderDataset
from nvae.utils import add_sn
from nvae.ngon_celeba import NGON
from tqdm import tqdm, contrib, notebook
import torch as T
import cv2

LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)
class train_config():
    
    def __init__(self):
        self.epochs = 500
        self.batch_size = 16
        self.dataset_path = "/home/tim/UTKFace"
        self.pretrained_weights = False
        self.n_cpu = 16
        self.notebook = False
        self.loop = 3

def train():
    opt = train_config()

    epochs = opt.epochs
    batch_size = opt.batch_size
    dataset_path = opt.dataset_path
    if opt.notebook:
        itering = notebook.tqdm
    else:
        itering = tqdm

    train_ds = ImageFolderDataset(dataset_path, img_dim=64)
    train_dataloader = DataLoader(train_ds, batch_size=batch_size, shuffle=True, num_workers=opt.n_cpu)

    os.makedirs("checkpoints", exist_ok=True)
    os.makedirs("output", exist_ok=True)

    device = "cuda:1" if torch.cuda.is_available() else "cpu"

    z_dim = 64
    img_dim = 64
    
    model = NGON(z_dim=512, img_dim=img_dim, latent = z_dim, M_N=opt.batch_size / len(train_ds))

    # apply Spectral Normalization
    #model.apply(add_sn)

    model.to(device)

    if opt.pretrained_weights:
        model.load_state_dict(torch.load(opt.pretrained_weights, map_location=device), strict=False)

    optimizer = torch.optim.Adamax(model.parameters(), lr=0.001)
    scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=15)
    
    for epoch in itering(range(epochs)):
        model.train()

        n_true = 0
        total_size = 0
        total_loss = 0
        iter_ = itering(train_dataloader)
        for i, image in enumerate(iter_):
            #optimizer.zero_grad()

            image = image.to(device)
            
            # x, z_origins, Batch, z_dim, img_dim
            
            z_origin = [
                torch.zeros((image.shape[0], z_dim), dtype=torch.float32).to(device).requires_grad_(),
                torch.zeros((image.shape[0], z_dim), dtype=torch.float32).to(device),#.requires_grad_(),
                torch.zeros((image.shape[0], z_dim), dtype=torch.float32).to(device),#.requires_grad_()
            ]
            
            inner_image_recon, inner_loss = model(image, z_origin, image.shape[0], 512, kl_step=False)
            grad0 = T.autograd.grad(inner_loss, [z_origin[0]], create_graph=True, retain_graph=True)[0]
            z_origin[0] = z_origin[0] - grad0
            #z_origin[0].requires_grad_(False)
            z_origin[1].requires_grad_(True)
            z_origin[2].requires_grad_(True)
            
            for i in range(opt.loop):
                middle_image_recon, middle_loss = model(image, z_origin, image.shape[0], 512, kl_step=False)
                grad1 = T.autograd.grad(middle_loss, [z_origin[1]], create_graph=True, retain_graph=True)[0]
                grad2 = T.autograd.grad(middle_loss, [z_origin[2]], create_graph=True, retain_graph=True)[0]
                z_origin[1] = z_origin[1] - grad1
                z_origin[2] = z_origin[2] - grad2
                
                
            #for z in z_origin:
                #grad = T.autograd.grad(inner_loss, [z], create_graph=True, retain_graph=True)[0]
                #z = (-grad)
                #new_z_origin.append(z)
            middle_image_recon, middle_loss = model(image, z_origin, image.shape[0], 512)
            grad0 = T.autograd.grad(middle_loss, [z_origin[0]], create_graph=True, retain_graph=True)[0]
            z_origin[0] = z_origin[0] - grad0
            image_recon, outer_loss = model(image, z_origin, image.shape[0], 512)
            


            #loss.backward()
            optimizer.zero_grad()
            outer_loss.backward()
            optimizer.step()
            total_loss += outer_loss.item() #loss.item()
            total_size += 1
            iter_.set_description(f'total_loss:{round(total_loss/total_size, 4)},  epoch {epoch}, lr:{0.001}')
        scheduler.step(epoch)
        
        torch.save(model.state_dict(), f"checkpoints/ae_ckpt_%d_%.6f.pth" % (epoch, total_loss/total_size))

        model.eval()

        with torch.no_grad():
            x0 = torch.zeros(1, 3, 64, 64).to(device)
            z = [
                torch.randn((1, z_dim)).to(device),
                torch.randn((1, z_dim)).to(device),
                torch.randn((1, z_dim)).to(device)
            ]
            gen_img, _ = model(x0, z, 1, 512)
            gen_img = gen_img.permute(0, 2, 3, 1)
            gen_img = gen_img[0].cpu().numpy()  * 255
            gen_img = gen_img.astype(np.uint8)

            #plt.imshow(gen_img)
            plt.savefig(f"output/ae_ckpt_%d_%.6f.png" % (epoch, total_loss))
            #plt.show()

if __name__ == "__main__":
    train()